import express,{Request,Response} from 'express';
import centreService from './../services/centre-service';
import asyncWrapper from './wrapper';


const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const data = await centreService.findAll();
    res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
    res.setHeader('X-Total-Count', 5);
    res.status(200).json(data.rows);
}),
);


router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id
    const data = await centreService.getOneById(id);
    res.status(200).json(data.rows);
}),
);


router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
    const centre = req.body
    const data = await centreService.create(centre);
    res.status(200).json(data.rows);
}),
);


router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const centre = req.body;
    const data = await centreService.update(id,centre);
    res.status(201).json(data.rows);
}),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const data = await centreService.remove(id);
    res.status(200).json(data.rows);
}),
);

export = router;