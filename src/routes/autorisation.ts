import express,{Request,Response} from 'express';
import autorisationService from '../services/autorisation-service';
import asyncWrapper from './wrapper';

const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
        const data = await autorisationService.findAll();
        res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
        res.setHeader('X-Total-Count', data.rowCount);
        res.status(200).json(data.rows);
    }),
);

router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
        const id = +req.params.id
        const data = await autorisationService.getOneById(id);
        res.status(200).json(data.rows);
    }),
);

router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
        const autorisation = req.body
        const data = await autorisationService.create(autorisation);
        res.status(200).json(data.rows);
    }),
);

router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
        const id = +req.params.id;
        const autorisation = req.body;
        const data = await autorisationService.update(id,autorisation);
        res.status(201).json(data.rows);
    }),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
        const id = +req.params.id;
        const data = await autorisationService.remove(id);
        res.status(200).json(data.rows);
    }),
);

export = router;