import express,{Request,Response} from 'express';
import automateService from './../services/automate-service';
import asyncWrapper from './wrapper';

const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
        const data = await automateService.findAll();
        res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
        res.setHeader('X-Total-Count', data.rowCount);
        res.status(200).json(data);
    }),
);

router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
        const id = +req.params.id
        const data = await automateService.getOneById(id);
        res.status(200).json(data);
}),
);

router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
    const typeAutomate = req.body
    const data = await automateService.create(typeAutomate);
    res.status(200).json(data);
}),
);

router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
        const id = +req.params.id;
        const typeAutomate = req.body;
        const data = await automateService.update(id,typeAutomate);
        res.status(201).json(data);
}),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
        const id = +req.params.id;
        const data = await automateService.remove(id);
        res.status(200).json(data);
    }),
);

export = router;