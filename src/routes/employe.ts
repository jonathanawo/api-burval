import express,{Request,Response} from 'express';
import employeService from '../services/employe-service';
import asyncWrapper from './wrapper';


const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const data = await employeService.findAll();
    res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
    res.setHeader('X-Total-Count', data.rowCount);
    res.status(200).json(data.rows);
}),
);

router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id
    const data = await employeService.getOneById(id);
    res.status(200).json(data.rows);
}),
);

router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
    const employe = req.body
    const data = await employeService.create(employe);
    res.status(200).json(data.rows);
}),
);

router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const employe = req.body;
    const data = await employeService.update(id,employe);
    res.status(201).json(data.rows);
}),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const data = await employeService.remove(id);
    res.status(200).json(data.rows);
}),
);

export = router;