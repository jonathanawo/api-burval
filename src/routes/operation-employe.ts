import express,{Request,Response} from 'express';
import operationUserService from '../services/operation-employe-service';
import asyncWrapper from './wrapper';


const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const data = await operationUserService.findAll();
    res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
    res.setHeader('X-Total-Count', data.rowCount);
    res.status(200).json(data.rows);
}),
);


router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id
    const data = await operationUserService.getOneById(id);
    res.status(200).json(data.rows);
}),
);


router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
    const opuse = req.body
    const data = await operationUserService.create(opuse);
    res.status(200).json(data.rows);
}),
);


router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const opuse = req.body;
    const data = await operationUserService.update(id,opuse);
    res.status(201).json(data.rows);
}),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const data = await operationUserService.remove(id);
    res.status(200).json(data.rows);
}),
);

export = router;