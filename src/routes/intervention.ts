import express,{Request,Response} from 'express';
import interventionService from '../services/intervention-service';
import asyncWrapper from './wrapper';


const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const data = await interventionService.findAll();
    res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
    res.setHeader('X-Total-Count', data.rowCount);
    res.status(200).json(data.rows);
}),
);

router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id
    const data = await interventionService.getOneById(id);
    res.status(200).json(data.rows);
}),
);

router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
    const intervention = req.body
    const data = await interventionService.create(intervention);
    res.status(200).json(data.rows);
}),
);

router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const intervention = req.body;
    const data = await interventionService.update(id,intervention);
    res.status(201).json(data.rows);
}),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const data = await interventionService.remove(id);
    res.status(200).json(data.rows);
}),
);

export = router;