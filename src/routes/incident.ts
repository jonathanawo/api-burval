import express,{Request,Response} from 'express';
import incidentService from '../services/incident-service';
import asyncWrapper from './wrapper';


const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const data = await incidentService.findAll();
    res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
    res.setHeader('X-Total-Count', data.rowCount);
    res.status(200).json(data.rows);
}),
);


router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id
    const data = await incidentService.getOneById(id);
    res.status(200).json(data.rows);
}),
);


router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
    const incident = req.body
    const data = await incidentService.create(incident);
    res.status(200).json(data.rows);
}),
);


router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const incident = req.body;
    const data = await incidentService.update(id,incident);
    res.status(201).json(data.rows);
}),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const data = await incidentService.remove(id);
    res.status(200).json(data.rows);
}),
);

export = router;