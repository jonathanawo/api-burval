import {Express} from 'express';
import regionRoutes from './region';
import centreRoutes from './centre';
import banqueRoutes from './banque';
import siteRoutes from './site';
import interventionRoutes from './intervention';
import banqueAutomateRoutes from './banque-automate';
import moduleRoutes from './module';
import incidentRoutes from './incident';
import automateRoutes from './automate';
import operationRoutes from './operation';
import employeRoutes from './employe';
import operationEmployeRoutes from './operation-employe';
import autorisationRoutes from './autorisation';


const loadAllRoutes = (app : Express) => {
    app.use('/api/regions',regionRoutes);
    app.use('/api/centres',centreRoutes);
    app.use('/api/banques',banqueRoutes);
    app.use('/api/sites', siteRoutes);
    app.use('/api/interventions', interventionRoutes);
    app.use('/api/banqueAutomates', banqueAutomateRoutes);
    app.use('/api/modules', moduleRoutes);
    app.use('/api/incidents', incidentRoutes);
    app.use('/api/automates', automateRoutes);
    app.use('/api/operations', operationRoutes);
    app.use('/api/employes', employeRoutes);
    app.use('/api/autorisations', autorisationRoutes);
    app.use('/api/operationemployes', operationEmployeRoutes);
}

export = loadAllRoutes;