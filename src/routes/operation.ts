import express,{Request,Response} from 'express';
import operationService from './../services/operation-service';
import asyncWrapper from './wrapper';


const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const data = await operationService.findAll();
    res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
    res.setHeader('X-Total-Count', data.rowCount);
    res.status(200).json(data.rows);
}),
);


router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id
    const data = await operationService.getOneById(id);
    res.status(200).json(data.rows);
}),
);


router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
    const operation = req.body
    const data = await operationService.create(operation);
    res.status(200).json(data.rows);
}),
);


router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const operation = req.body;
    const data = await operationService.update(id,operation);
    res.status(201).json(data.rows);
}),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const data = await operationService.remove(id);
    res.status(200).json(data.rows);
}),
);

export = router;