import express,{Request,Response} from 'express';
import banqueAutomateService from '../services/banque-automate-service';
import asyncWrapper from './wrapper';


const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const data = await banqueAutomateService.findAll();
    res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
    res.setHeader('X-Total-Count', data.rowCount);
    res.status(200).json(data.rows);
}),
);


router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id
    const data = await banqueAutomateService.getOneById(id);
    res.status(200).json(data.rows);
}),
);


router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
    const bta = req.body
    const data = await banqueAutomateService.create(bta);
    res.status(200).json(data.rows);
}),
);


router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const bta = req.body;
    const data = await banqueAutomateService.update(id,bta);
    res.status(201).json(data.rows);
}),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const data = await banqueAutomateService.remove(id);
    res.status(200).json(data.rows);
}),
);

export = router;