import express,{Request,Response} from 'express';
import siteService from '../services/site-service';
import asyncWrapper from './wrapper';


const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
        const data = await siteService.findAll();
        res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
        res.setHeader('X-Total-Count', data.rowCount);
        res.status(200).json(data.rows);
    }),
);


router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
        const id = +req.params.id
        const data = await siteService.getOneById(id);
        res.status(200).json(data.rows);
    }),
);


router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
        const site = req.body
        const data = await siteService.create(site);
        res.status(200).json(data.rows);
    }),
);


router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
        const id = +req.params.id;
        const site = req.body;
        const data = await siteService.update(id,site);
        res.status(201).json(data.rows);
    }),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
        const id = +req.params.id;
        const data = await siteService.remove(id);
        res.status(200).json(data.rows);
    }),
);

export = router;