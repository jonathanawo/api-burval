import express,{Request,Response} from 'express';
import moduleService from './../services/module-service';
import asyncWrapper from './wrapper';


const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const data = await moduleService.findAll();
    res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
    res.setHeader('X-Total-Count', data.rowCount);
    res.status(200).json(data.rows);
}),
);

router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id
    const data = await moduleService.getOneById(id);
    res.status(200).json(data.rows);
}),
);

router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
    const module = req.body
    const data = await moduleService.create(module);
    res.status(200).json(data.rows);
}),
);

router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const module = req.body;
    const data = await moduleService.update(id,module);
    res.status(201).json(data.rows);
}),
);


router.route('/:id').delete(
    asyncWrapper(
    async (req: Request, res: Response) => {
    const id = +req.params.id;
    const data = await moduleService.remove(id);
    res.status(200).json(data.rows);
}),
);

export = router;