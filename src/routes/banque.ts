import express, {Request,Response} from 'express';
import banqueService from '../services/banque-service';
import asyncWrapper from './wrapper';


const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const data = await banqueService.findAll();
    res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
    res.setHeader('X-Total-Count', data.rowCount);
    res.status(200).json(data.rows);
}),
);

router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id
    const data = await banqueService.getOneById(id);
    res.status(200).json(data.rows);
}),
);

router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
    const banque = req.body
    const data = await banqueService.create(banque);
    res.status(200).json(data.rows);
}),
);

router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const banque = req.body;
    const data = await banqueService.update(id,banque);
    res.status(201).json(data.rows);
}),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id;
    const data = await banqueService.remove(id);
    res.status(200).json(data.rows);
}),
);

export = router;