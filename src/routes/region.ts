import express,{Request,Response} from 'express';
import regionService from './../services/region-service';
import asyncWrapper from './wrapper';


const router = express.Router();

router.route('/').get(
    asyncWrapper(async (req: Request, res: Response) => {
        const data = await regionService.findAll();
        res.setHeader('Access-Control-Expose-Headers', 'X-Total-Count');
        res.setHeader('X-Total-Count', data.rowCount);
        res.status(200).json(data.rows);
    })
);

router.route('/:id').get(
    asyncWrapper(async (req: Request, res: Response) => {
    const id = +req.params.id
    const data = await regionService.getOneById(id);
    res.status(200).json(data.rows);
}),
);

router.route('/').post(
    asyncWrapper(async (req: Request, res: Response) => {
    const region = req.body
    const data = await regionService.create(region);
    res.status(200).json(data.rows);
}),
);

router.route('/:id').put(
    asyncWrapper(async (req: Request, res: Response) => {
        const id = +req.params.id;
        const region = req.body;
        const data = await regionService.update(id,region);
        res.status(201).json(data.rows);
    }),
);


router.route('/:id').delete(
    asyncWrapper(async (req: Request, res: Response) => {
        const id = +req.params.id;
        const data = await regionService.remove(id);
        res.status(200).json(data.rows);
    }),
);

export = router;