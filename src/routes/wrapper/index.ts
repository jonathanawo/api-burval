import {Request, Response} from 'express';
import validator from 'validator';

export default function asyncWrapper(callback: any) {
  return function(req: Request, res: Response, next: any) {
    callback(req, res, next).catch((err: Error) => {
      console.log(err.message);
      let code = 9000; // autres
      if (validator.isJSON(err.message)) {
        const errorJson = JSON.parse(err.message);
        if (errorJson.type === 'db') {
          code = 8000; // erreur base de données
        }
      }
      // TODO: logger l'erreur ici
      next(new Error(code + ''));
    });
  };
}
