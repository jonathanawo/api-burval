import { BanqueAutomate } from '../models/banque-automate';
import banqueAutomateRepository from '../db/banque-automate';

const findAll = async () => {
    return await banqueAutomateRepository.listBanqueAutomates();
}

const getOneById = async (id:number) => {
    return await banqueAutomateRepository.findBanqueAutomate(id);
}

const create = async (banqueAutomate:BanqueAutomate) => {
    return await banqueAutomateRepository.addBanqueAutomate(banqueAutomate);
}

const update = async (id:number, banqueAutomate:BanqueAutomate) => {
    return await banqueAutomateRepository.updateBanqueAutomate(id,banqueAutomate);
}

const remove = async (id:number) => {
    return await banqueAutomateRepository.deleteBanqueAutomate(id);
}

const findAllBanquesByAutomate = async (idAutomate:number) => {
    return await banqueAutomateRepository.findListBanqueByAutomate(idAutomate);
}

const findAllAutomatesByBanque = async (idBanque:number) => {
    return await banqueAutomateRepository.findListBanqueByAutomate(idBanque);
}

export = {
    findAll,
    getOneById,
    create,
    update,
    remove,
    findAllBanquesByAutomate,
    findAllAutomatesByBanque
}