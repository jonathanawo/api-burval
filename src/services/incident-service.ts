import { Incident } from '../models/incident';
import incidentRepository from '../db/incident';

const findAll = async () => {
    return await incidentRepository.listIncidents();
}

const getOneById = async (id:number) => {
    return await incidentRepository.findIncident(id);
}

const create = async (incident : Incident) => {
    return await incidentRepository.addIncident(incident);
}

const update = async (id:number,incident:Incident) => {
    return await incidentRepository.updateIncident(id,incident);
}

const remove = async (id:number) => {
    return await incidentRepository.deleteIncident(id);
}

export = {
    findAll,
    getOneById,
    create,
    update,
    remove
}