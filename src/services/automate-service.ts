import { Automate } from '../models/automate';
import AutomateRepository from '../db/automate';

const findAll = async () => {
    return await AutomateRepository.listAutomates();
}

const getOneById = async (id:number) => {
    return await AutomateRepository.findAutomate(id);
}

const create = async (Automate : Automate) => {
    return await AutomateRepository.addAutomate(Automate);
}

const update = async (id:number,Automate:Automate) => {
    return await AutomateRepository.updateAutomate(id,Automate);
}

const remove = async (id:number) => {
    return await AutomateRepository.deleteAutomate(id);
}

export = {
    findAll,
    getOneById,
    create,
    update,
    remove
}