import { Site } from './../models/site';
import siteRepository from './../db/site';

const findAll = async () => {
    return await siteRepository.listSites();
}

const getOneById = async (id:number) => {
    return await siteRepository.findSite(id);
}

const create = async (site : Site) => {
    return await siteRepository.addSite(site);
}

const update = async (id:number,site:Site) => {
    return await siteRepository.updateSite(id,site);
}

const remove = async (id:number) => {
    return await siteRepository.deleteSite(id);
}

export = {
    findAll,
    getOneById,
    create,
    update,
    remove
}