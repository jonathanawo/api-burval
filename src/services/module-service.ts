import { Module } from './../models/module';
import moduleRepository from './../db/module';

const findAll = async () => {
    return await moduleRepository.listModules();
}

const getOneById = async (id:number) => {
    return await moduleRepository.findModule(id);
}

const create = async (module : Module) => {
    return await moduleRepository.addModule(module);
}

const update = async (id:number,module:Module) => {
    return await moduleRepository.updateModule(id,module);
}

const remove = async (id:number) => {
    return await moduleRepository.deleteModule(id);
}

export = {
    findAll,
    getOneById,
    create,
    update,
    remove
}