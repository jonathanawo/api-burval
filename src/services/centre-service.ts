import { Centre } from '../models/centre';
import centreRepository from '../db/centre';

const findAll = async () => {
    return await centreRepository.listCentres();
}

const getOneById = async (id:number) => {
    return await centreRepository.findCentre(id);
}

const create = async (centre : Centre) => {
    return await centreRepository.addCentre(centre);
}

const update = async (id:number,centre:Centre) => {
    return await centreRepository.updateCentre(id,centre);
}

const remove = async (id:number) => {
    return await centreRepository.deleteCentre(id);
}

export = {
    findAll,
    getOneById,
    create,
    update,
    remove
}