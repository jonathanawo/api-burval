import { Region } from './../models/region';
import regionRepository from './../db/region';

const findAll = async () => {
    return await regionRepository.listRegions();
}

const getOneById = async (id:number) => {
    return await regionRepository.findRegion(id);
}

const create = async (region : Region) => {
    return await regionRepository.addRegion(region);
}

const update = async (id:number,region:Region) => {
    return await regionRepository.updateRegion(id,region);
}

const remove = async (id:number) => {
    return await regionRepository.deleteRegion(id);
}

export = {
    findAll,
    getOneById,
    create,
    update,
    remove
}