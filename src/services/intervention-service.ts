import { Intervention } from '../models/intervention';
import interventionRepository from '../db/intervention';

const findAll = async () => {
    return await interventionRepository.listIntervention();
}

const getOneById = async (id:number) => {
    return await interventionRepository.findIntervention(id);
}

const create = async (Intervention : Intervention) => {
    return await interventionRepository.addIntervention(Intervention);
}

const update = async (id:number, Intervention:Intervention) => {
    return await interventionRepository.updateIntervention(id,Intervention);
}

const remove = async (id:number) => {
    return await interventionRepository.deleteIntervention(id);
}

export = {
    findAll,
    getOneById,
    create,
    update,
    remove
}