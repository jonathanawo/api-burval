import { OperationEmploye } from '../models/operation-employe';
import operationEmployeRepository from '../db/operation-employe';

const findAll = async () => {
    return await operationEmployeRepository.listOperationEmployes();
}

const getOneById = async (id:number) => {
    return await operationEmployeRepository.findOperationEmploye(id);
}

const create = async (opus : OperationEmploye) => {
    return await operationEmployeRepository.addOperationEmploye(opus);
}

const update = async (id:number,opuse:OperationEmploye) => {
    return await operationEmployeRepository.updateOperationEmploye(id,opuse);
}

const remove = async (id:number) => {
    return await operationEmployeRepository.deleteOperationEmploye(id);
}

export = {
    findAll,
    getOneById,
    create,
    update,
    remove
}