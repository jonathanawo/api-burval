import { Banque } from '../models/banque';
import banqueRepository from '../db/banque';

const findAll = async () => {
    return await banqueRepository.listBanques();
}

const getOneById = async (id:number) => {
    return await banqueRepository.findBanque(id);
}

const create = async (banque : Banque) => {
    return await banqueRepository.addBanque(banque);
}

const update = async (id:number,banque:Banque) => {
    return await banqueRepository.updateBanque(id,banque);
}

const remove = async (id:number) => {
    return await banqueRepository.deleteBanque(id);
}

export = {
    findAll,
    getOneById,
    create,
    update,
    remove
}