import { Autorisation } from '../models/autorisation';
import autorisationRepository from '../db/autorisation';

const findAll = async () => {
    return await autorisationRepository.listAutorisations();
}

const getOneById = async (id:number) => {
    return await autorisationRepository.findAutorisation(id);
}

const create = async (autorisation : Autorisation) => {
    return await autorisationRepository.addAutorisation(autorisation);
}

const update = async (id:number,autorisation:Autorisation) => {
    return await autorisationRepository.updateAutorisation(id,autorisation);
}

const remove = async (id:number) => {
    return await autorisationRepository.deleteAutorisation(id);
}

export = {
    findAll,
    getOneById,
    create,
    update,
    remove
}