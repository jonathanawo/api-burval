import { Employe } from '../models/employe';
import employeRepository from '../db/employe';
import bcrypt from 'bcrypt'

const findAll = async () => {
    return await employeRepository.listEmployes();
}

const getOneById = async (id:number) => {
    return await employeRepository.findEmploye(id);
}

const getOneByEmail = async (email : string) => {
    return await employeRepository.findEmployeByEmail(email);
}

const create = async (employe : Employe) => {
    const salt = await bcrypt.genSalt();
    const passwordcrypted = await bcrypt.hash(employe.motdepasse, salt);
    return await employeRepository.addEmploye({...employe, motdepasse: passwordcrypted});
}

const update = async (id:number,employe:Employe) => {
    return await employeRepository.updateEmploye(id,employe);
}

const remove = async (id:number) => {
    return await employeRepository.deleteEmploye(id);
}

export = {
    findAll,
    getOneById,
    getOneByEmail,
    create,
    update,
    remove
}