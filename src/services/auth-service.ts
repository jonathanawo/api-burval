import { Employe } from './../models/employe';
import employeRepository from '../db/employe'
import ApiResponse from '../models/api-response';
import bcrypt from 'bcrypt';
import utils from '../constants/utils';

const login = async ({email, motdepasse} : {email: string, motdepasse: string}) => {
    const errorMessage = 'Email ou mot de passe incorrect.';

    // Vérifier que l'utilisateur est en base 
    const account = await employeRepository.findEmployeByEmail(email);
    const emp = (account.rows[0] as any) as Employe
    if (!emp){
        return ApiResponse.withError(new Error(errorMessage));
    }

    // Comparer les mots de passe
    const match = bcrypt.compare(motdepasse, emp.motdepasse);
    if (!match){
        return ApiResponse.withError(new Error(errorMessage));
    }


    // Générer un token JWT
  const token = utils.generateTokenForDriver({
    id: emp.id,
    email: emp.email.toLowerCase(),
    user_role: `DABISTE`,
    aud: `${process.env.JWT_AUDIENCE}`,
  });

  return ApiResponse.withSuccess({token});


}