import { Operation } from './../models/operation';
import operationRepository from './../db/operation';

const findAll = async () => {
    return await operationRepository.listOperations();
}

const findAllNotStarted = async () => {
    return await operationRepository.listOperationsNonDemarrees();
}

const findAllStarted = async () => {
    return await operationRepository.listOperationsDemarrees();
}

const findAllFinished = async () => {
    return await operationRepository.listOperationsTerminees();
}

const getOneById = async (id:number) => {
    return await operationRepository.findOperation(id);
}

const create = async (operation : Operation) => {
    return await operationRepository.addOperation(operation);
}

const update = async (id:number,operation:Operation) => {
    return await operationRepository.updateOperation(id,operation);
}

const remove = async (id:number) => {
    return await operationRepository.deleteOperation(id);
}

export = {
    findAll,
    findAllNotStarted,
    findAllStarted,
    findAllFinished,
    getOneById,
    create,
    update,
    remove
}