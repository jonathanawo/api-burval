import jwt from 'jsonwebtoken';
import nodemailer from 'nodemailer';
import fs from 'fs';
import path from 'path';


const generateTokenForAdmin = (payload: string | object) => {
    const jwtSecret = `${process.env.JWT_SECRET}`;
    const access_token = jwt.sign(payload, jwtSecret, {
      algorithm: 'HS512',
      expiresIn: '24h',
    });
  
    return {
      access_token,
      expiresIn: 24 * 60 * 60,
    };
  };
  
  const generateTokenForDriver = (payload: string | object) => {
    const jwtSecret = `${process.env.JWT_SECRET}`;
    const access_token = jwt.sign(payload, jwtSecret, {
      algorithm: 'HS512',
      expiresIn: '1y',
    });
  
    return {
      access_token,
      expiresIn: 365 * 24 * 60 * 60,
    };
  };
  
  const generateActivationToken = (payload: string | object) => {
    const jwtSecret = `${process.env.JWT_SECRET}`;
    return jwt.sign(payload, jwtSecret, {
      algorithm: 'HS512',
      expiresIn: '2days',
    });
  };
  
  const getTemplate = (filename: string) => {
    const pathName = path.join(
      __dirname,
      '..',
      '..',
      'src',
      'templates',
      filename,
    );
    // console.log(pathName);
  
    try {
      return fs.readFileSync(pathName, 'utf8');
    } catch (error) {
      return null;
    }
  };
  
  const sendEmail = async (lastName: string, email: string, token: string) => {
    // let testAccount = await nodemailer.createTestAccount();
    let template = getTemplate('confirmation.html');
  
    // console.log(process.env.NODE_ENV);
    let domaine = 'localhost';
  
    if (!template) return;
  
    template = template
      .replace('${domaine}', domaine)
      .replace('${lastName}', lastName)
      .replace('${token}', token);
  
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
      host: 'ssl0.ovh.net',
      port: 587,
      secure: false, // true for 465, false for other ports
      auth: {
        user: 'no-reply@pcoursepro.fr', // generated ethereal user
        pass: '3uDk7Cu.6^V#', // generated ethereal password
      },
    });
  
    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: 'rubenawo@gmail.com', // sender address
      to: email,
      subject: 'Incident GAB/DAB ✔', // Subject line
      html: template, // html body
    });
  
    console.log('Message sent: %s', info.messageId);
  
    // Preview only available when sending through an Ethereal account
    // console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
  };

  export default {
    generateTokenForAdmin,
    generateTokenForDriver,
    generateActivationToken,
    sendEmail,
  };