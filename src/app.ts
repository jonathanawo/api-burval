import express, {Request, Response} from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import dotenv from 'dotenv';
import routes from './routes';
import passport from 'passport';
import helmet from 'helmet';
const app = express();
// Charger toutes les variables d'environnement
dotenv.config();

// Charger les middlwares
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

// Charger les routes
routes(app);
// Gestion des erreurs centralisées
app.use((error: any, req: Request, res: Response, next: any) => {
  let message = `Internal server error : ${error.message}`;
  res.status(500).json({message});
});
// Lancer l'application
const port = 8081;
app.listen(port, async () => {
  console.log(`Le serveur a démarré sur le port ${port}`);
});