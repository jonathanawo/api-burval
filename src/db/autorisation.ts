import { Autorisation } from '../models/autorisation';
import db from './connect';

const listAutorisations = () => {
    const query = `SELECT * FROM AUTORISATION ORDER BY libelle ASC`;
    return db.query(query);
}

const addAutorisation = (autorisation :Autorisation) => {
    const {libelle} = autorisation;
    const query = `INSERT INTO AUTORISATION (libelle) VALUES ($1) RETURNING *`;
    return db.query(query,[libelle]);
}

const updateAutorisation = (id: number, autorisation: Autorisation) => {
    const {libelle} = autorisation;    
    const query = `UPDATE AUTORISATION SET libelle = $2, derniereMiseAJour = NOW() WHERE id = $1 RETURNING *`;
    return db.query(query,[id,libelle]);
}

const deleteAutorisation = (id: number) => {
    const query = `DELETE AUTORISATION WHERE id = $1`;
    return db.query(query,[id]);
}

const findAutorisation = (id: number) => {
    const query = `SELECT * FROM AUTORISATION WHERE id = $1`;
    return db.query(query,[id]);
}

export = {
    listAutorisations,
    addAutorisation,
    updateAutorisation,
    deleteAutorisation,
    findAutorisation
}