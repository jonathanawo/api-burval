import { Incident } from './../models/incident';
import db from './connect';

const listIncidents = () => {
    const query = `SELECT * FROM INCIDENT ORDER BY libelle ASC`;
    return db.query(query);
}

const addIncident = (incident :Incident) => {
    const query = `INSERT INTO INCIDENT (libelle,idmodule) VALUES ($1,$2) RETURNING *`;
    return db.query(query,[incident.libelle,incident.idmodule]);
}

const updateIncident = (id: number, incident: Incident) => {
    const {libelle,idmodule} = incident;    
    const query = `UPDATE INCIDENT SET libelle = $2, idmodule = $3, derniereMiseAJour = NOW() WHERE id = $1`;
    return db.query(query,[id,libelle,idmodule]);
}

const deleteIncident = (id: number) => {
    const query = `DELETE INCIDENT WHERE id = $1`;
    return db.query(query,[id]);
}

const findIncident = (id: number) => {
    const query = `SELECT * FROM INCIDENT WHERE id = $1`;
    return db.query(query,[id]);
}

export = {
    listIncidents,
    addIncident,
    updateIncident,
    deleteIncident,
    findIncident
}