import { Module } from './../models/module';
import db from './connect';

const listModules = () => {
    const query = `SELECT * FROM MODULE WHERE libelle ASC`;
    return db.query(query);
}

const addModule = (module :Module) => {
    const {libelle} = module;
    const query = `INSERT INTO MODULE (libelle) values ($1) RETURNING *`;
    return db.query(query,[libelle]);
}

const updateModule = (id: number, module: Module) => {
    const {libelle} = module;    
    const query = `UPDATE MODULE SET libelle = $2, derniereMiseAJour = NOW() where id = $1 RETURNING *`;
    return db.query(query,[id,libelle]);
}

const deleteModule = (id: number) => {
    const query = `DELETE MODULE WHERE id = $1`;
    return db.query(query,[id]);
}

const findModule = (id: number) => {
    const query = `SELECT * FROM MODULE WHERE id = $1`;
    return db.query(query,[id]);
}

export = {
    listModules,
    addModule,
    updateModule,
    deleteModule,
    findModule
}