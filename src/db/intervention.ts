import { Intervention } from '../models/intervention';
import db from './connect';

const listIntervention = () => {
    const query = `SELECT * FROM INTERVENTION ORDER BY libelle ASC`;
    return db.query(query);
}

const addIntervention = (intervention :Intervention) => {
    const {libelle} = intervention;
    const query = `INSERT INTO INTERVENTION (libelle) VALUES ($1) RETURNING *`;
    return db.query(query,[libelle]);
}

const updateIntervention = (id: number, intervention: Intervention) => {
    const {libelle} = intervention;    
    const query = `UPDATE INTERVENTION SET libelle = $2 WHERE id = $1 RETURNING *`;
    return db.query(query,[id,libelle]);
}

const deleteIntervention = (id: number) => {
    const query = `DELETE INTERVENTION WHERE id = $1`;
    return db.query(query,[id]);
}

const findIntervention = (id: number) => {
    const query = `SELECT * FROM INTERVENTION WHERE id = $1`;
    return db.query(query,[id]);
}

export = {
    listIntervention,
    addIntervention,
    updateIntervention,
    deleteIntervention,
    findIntervention
}