import { Employe } from '../models/employe';
import db from './connect';

const listEmployes = () => {
    const query = `SELECT * FROM EMPLOYE ORDER BY nom,prenom ASC`;
    return db.query(query);
}

const addEmploye = (employe :Employe) => {
    const {email,motdepasse,nom,prenom,idautorisation} = employe;
    const query = `INSERT INTO EMPLOYE (email,motdepasse,nom,prenom,idautorisation) VALUES ($1,$2,$3,$4,$5) RETURNING *`;
    return db.query(query,[email,motdepasse,nom,prenom,idautorisation]);
}

const updateEmploye = (id: number, employe: Employe) => {
    const query = `UPDATE EMPLOYE SET EMAIL = $2, MOTDEPASSE = $3,NOM = $4,PRENOM = $5,IDAUTORISATION = $6,derniereMiseAJour = NOW() where id = $1 RETURNING *`;
    return db.query(query,[id,employe.email,employe.motdepasse,employe.nom,employe.prenom,employe.idautorisation]);
}

const deleteEmploye = (id: number) => {
    const query = `delete employe where id = $1`;
    return db.query(query,[id]);
}

const findEmploye = (id: number) => {
    const query = `select * from employe where id = $1`;
    return db.query(query,[id]);
}

const findEmployeByEmail = (email: string) => {
    const query = `SELECT * FROM employe WHERE trim(from lower(email)) = trim(from lower($1)) LIMIT 1`;
    return db.query(query, [email]);
  };

export = {
    listEmployes,
    addEmploye,
    updateEmploye,
    deleteEmploye,
    findEmploye,
    findEmployeByEmail
}