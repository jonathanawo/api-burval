import { BanqueAutomate } from '../models/banque-automate';
import db from './connect';

const listBanqueAutomates = () => {
    const query = `SELECT * FROM BANQUEAUTOMATE ORDER BY idbanque,idautomate ASC`;
    return db.query(query);
}

const addBanqueAutomate = (banqueAutomate :BanqueAutomate) => {
    const query = `INSERT INTO BANQUEAUTOMATE (idbanque,idautomate) VALUES ($1,$2) RETURNING *`;
    return db.query(query,[banqueAutomate.idbanque,banqueAutomate.idautomate]);
}

 const updateBanqueAutomate = (id: number, banqueAutomate :BanqueAutomate) => {
    const {idbanque , idautomate} = banqueAutomate;    
    const query = `UPDATE BANQUEAUTOMATE SET idbanque = $2, idautomate = $3, derniereMiseAJour = NOW() WHERE id = $1 RETURNING *`;
    return db.query(query,[id, idbanque, idautomate]);
} 

const deleteBanqueAutomate = (id: number) => {
    const query = `DELETE BANQUEAUTOMATE WHERE id = $1`;
    return db.query(query,[id]);
}

const findBanqueAutomate = (id: number) => {
    const query = `SELECT * FROM BANQUEAUTOMATE WHERE id = $1`;
    return db.query(query,[id]);
}

const findListAutomateByBanque = (idBanque: number) => {
    const query = `SELECT idAutomate FROM BANQUEAUTOMATE WHERE idbanque = $1`;
    return db.query(query,[idBanque]);
}

const findListBanqueByAutomate = (idAutomate: number) => {
    const query = `SELECT idbanque FROM BANQUEAUTOMATE WHERE idautomate = $1`;
    return db.query(query,[idAutomate]);
}



export = {
    listBanqueAutomates,
    addBanqueAutomate,
    updateBanqueAutomate,
    deleteBanqueAutomate,
    findBanqueAutomate,
    findListAutomateByBanque,
    findListBanqueByAutomate
}