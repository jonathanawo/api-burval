import { Automate } from '../models/automate';
import db from './connect';

const listAutomates = () => {
    const query = `SELECT * FROM AUTOMATE ORDER BY libelle ASC`;
    return db.query(query);
}

const addAutomate = (automate :Automate) => {
    const {libelle} = automate;
    const query = `INSERT INTO AUTOMATE (libelle) VALUES ($1) RETURNING *`;
    return db.query(query,[libelle]);
}

const updateAutomate = (id: number, automate: Automate) => {
    const {libelle} = automate;    
    const query = `UPDATE AUTOMATE SET libelle = $2, derniereMiseAJour = NOW() where id = $1 RETURNING *`;
    return db.query(query,[id,libelle]);
}

const deleteAutomate = (id: number) => {
    const query = `DELETE AUTOMATE WHERE id = $1`;
    return db.query(query,[id]);
}

const findAutomate = (id: number) => {
    const query = `SELECT * FROM AUTOMATE WHERE id = $1`;
    return db.query(query,[id]);
}

export = {
    listAutomates,
    addAutomate,
    updateAutomate,
    deleteAutomate,
    findAutomate
}