import { Operation } from './../models/operation';
import db from './connect';
import moment from 'moment';

const listOperations = () => {
    const query = `SELECT * FROM OPERATION ORDER BY datedeclaration DESC`;
    return db.query(query);
}

const listOperationsNonDemarrees = () => {
    const query = `SELECT * FROM OPERATION WHERE datedebintervention IS NULL OR trim(datedebintervention)='' ORDER BY datedeclaration desc`;
    return db.query(query);
}

const listOperationsDemarrees = () => {
    const query = `SELECT * FROM OPERATION WHERE datedebintervention IS NOT NULL AND (datefinintervention IS NULL OR trim(datefinintervention)='') ORDER BY datedeclaration desc`;
    return db.query(query);
}

const listOperationsTerminees = () => {
    const query = `SELECT * FROM OPERATION WHERE datefinintervention IS NOT NULL ORDER BY datedeclaration desc`;
    return db.query(query);
}

const addOperation = (operation :Operation) => {
    const {
        description,numbordereau,datedeclaration,datereponse,
        datearrivee,datedebintervention,datefinintervention,
        idsite,idautomate,
        idintervention,idincident
    } = operation;

    const query = `INSERT INTO OPERATION (
        description,numbordereau,datedeclaration,datereponse,
        datearrivee,datedebintervention,datefinintervention,
        idsite,idautomate,
        idintervention,idincident
    ) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) RETURNING *`;
    return db.query(query,[
        description,numbordereau,datedeclaration,datereponse,
        datearrivee,datedebintervention,datefinintervention,
        idsite,idautomate,
        idintervention,idincident
    ]);
}

const updateOperation = (id: number, operation: Operation) => {
    const {
        description,numbordereau,datedeclaration,datereponse,
        datearrivee,datedebintervention,datefinintervention,
        idsite,idautomate,
        idintervention,idincident
    } = operation;    
    const query = `UPDATE OPERATION SET description = $2, numbordereau = $3, datedeclaration = $4 
    ,datereponse = $5 , datearrivee = $6 , datedebintervention = $7 , datefinintervention = $8,
    idsite = $9 , idautomate = $10 , idintervention = $11,
    idincident = $12 where id = $1 RETURNING *`;
    return db.query(query,[id,description,numbordereau,datedeclaration,datereponse,
        datearrivee,datedebintervention,datefinintervention,
        idsite,idautomate,
        idintervention,idincident]);
}

const updateDateArriveeOperation = (id: number, datearrivee: Date) => {
    const query = `UPDATE OPERATION SET datearrivee = NOW() WHERE id = $1 RETURNING *`;
}

const updateDateDebutIntervention = (id: number, datedebut: Date) => {
    const query = `UPDATE OPERATION SET datedebintervention = NOW() WHERE id = $1 RETURNING *`
}

const updateDateFinIntervention = (id: number, datefin: Date) => {
    const query = `UPDATE OPERATION SET datefinintervention = NOW() WHERE id = $1 RETURNING *`
}

const deleteOperation = (id: number) => {
    const query = `DELETE OPERATION WHERE id = $1`;
    return db.query(query,[id]);
}

const findOperation = (id: number) => {
    const query = `SELECT * FROM OPERATION WHERE id = $1`;
    return db.query(query,[id]);
}

export = {
    listOperations,
    listOperationsNonDemarrees,
    listOperationsDemarrees,
    listOperationsTerminees,
    addOperation,
    updateOperation,
    updateDateArriveeOperation,
    updateDateDebutIntervention,
    updateDateFinIntervention,
    deleteOperation,
    findOperation
}