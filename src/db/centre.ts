import { Centre } from './../models/centre';
import db from './connect';

const listCentres = () => {
    const query = `SELECT * FROM CENTRE ORDER BY libelle ASC`;
    return db.query(query);
}

const addCentre = (centre :Centre) => {
    const {libelle,idregion} = centre;
    const query = `INSERT INTO CENTRE (libelle,idregion) VALUES ($1,$2) RETURNING *`;
    return db.query(query,[libelle,idregion]);
}

const updateCentre = (id: number, centre: Centre) => {
    const {libelle,idregion} = centre;    
    const query = `UPDATE CENTRE SET libelle = $2, idregion = $3, derniereMiseAJour = NOW() WHERE id = $1 RETURNING *`;
    return db.query(query,[id,libelle,idregion]);
}

const deleteCentre = (id: number) => {
    const query = `DELETE CENTRE WHERE id = $1`;
    return db.query(query,[id]);
}

const findCentre = (id: number) => {
    const query = `SELECT * FROM CENTRE WHERE id = $1`;
    return db.query(query,[id]);
}

export = {
    listCentres,
    addCentre,
    updateCentre,
    deleteCentre,
    findCentre
}