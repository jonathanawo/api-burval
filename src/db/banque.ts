import { Banque } from '../models/banque';
import db from './connect';

const listBanques = () => {
    const query = `SELECT * FROM BANQUE ORDER BY libelle ASC`;
    return db.query(query);
}

const addBanque = (banque :Banque) => {
    const {libelle} = banque;
    const query = `INSERT INTO BANQUE (libelle) VALUES ($1) RETURNING *`;
    return db.query(query,[libelle]);
}

const updateBanque = (id: number, banque: Banque) => {
    const {libelle} = banque;    
    const query = `UPDATE BANQUE SET libelle = $2, derniereMiseAJour = NOW() WHERE id = $1 RETURNING *`;
    return db.query(query,[id,libelle]);
}

const deleteBanque = (id: number) => {
    const query = `DELET BANQUE WHERE id = $1`;
    return db.query(query,[id]);
}

const findBanque = (id: number) => {
    const query = `SELECT * FROM BANQUE WHERE id = $1`;
    return db.query(query,[id]);
}

export = {
    listBanques,
    addBanque,
    updateBanque,
    deleteBanque,
    findBanque
}