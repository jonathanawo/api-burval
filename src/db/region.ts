import { Region } from './../models/region';
import db from './connect';

const listRegions = () => {
    const query = `select * from region order by id ASC`;
    return db.query(query);
}

const addRegion = (region :Region) => {
    const {libelle} = region;
    const query = `insert into region (libelle) values ($1)`;
    return db.query(query,[libelle]);
}

const updateRegion = (id: number, region: Region) => {
    const {libelle} = region;    
    const query = `update region set libelle = $2 where id = $1`;
    return db.query(query,[id,libelle]);
}

const deleteRegion = (id: number) => {
    const query = `delete region where id = $1`;
    return db.query(query,[id]);
}

const findRegion = (id: number) => {
    const query = `select * from region where id = $1`;
    return db.query(query,[id]);
}

export = {
    listRegions,
    addRegion,
    updateRegion,
    deleteRegion,
    findRegion
}