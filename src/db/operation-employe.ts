import { OperationEmploye } from './../models/operation-employe';
import db from './connect';

const listOperationEmployes = () => {
    const query = `select * from operationEmploye order by id ASC`;
    return db.query(query);
}

const addOperationEmploye = (operationUser :OperationEmploye) => {
    const {idoperation,idemploye} = operationUser;
    const query = `insert into operationUser (idoperation,idemploye) values ($1,$2)`;
    return db.query(query,[idoperation,idemploye]);
}

const updateOperationEmploye = (id: number, operationUser: OperationEmploye) => {
    const {idoperation,idemploye} = operationUser;    
    const query = `update operationUser set idoperation = $2, idemploye = $3 where id = $1`;
    return db.query(query,[id,idoperation,idemploye]);
}

const deleteOperationEmploye = (id: number) => {
    const query = `delete operationEmploye where id = $1`;
    return db.query(query,[id]);
}

const findOperationEmploye = (id: number) => {
    const query = `select * from operationEmploye where id = $1`;
    return db.query(query,[id]);
}

export = {
    listOperationEmployes,
    addOperationEmploye,
    updateOperationEmploye,
    deleteOperationEmploye,
    findOperationEmploye
}