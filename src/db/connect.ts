import {Pool} from 'pg';
// Charge la chaine de connexion automatiquement du fichier .env
const pool = new Pool();

export = {
    query: <T = any[]>(text: any, params?: any) => pool.query<T>(text, params),
    // query: (text: any, params?: any) => pool.query(text, params),
    
  };
