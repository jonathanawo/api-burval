import { Site } from './../models/site';
import db from './connect';

const listSites = () => {
    const query = `select * from site order by id ASC`;
    return db.query(query);
}

const addSite = (site :Site) => {
    //const {libelle,idregion} = site;
    const query = `insert into site (libelle,idbanque,idcentre) values ($1,$2,$3)`;
    return db.query(query,[site.libelle,site.idbanque,site.idcentre]);
}

const updateSite = (id: number, site: Site) => {
    const {libelle,idbanque,idcentre} = site;    
    const query = `update site set libelle = $2, idbanque = $3, idcentre = $4 where id = $1`;
    return db.query(query,[id,libelle,idbanque,idcentre]);
}

const deleteSite = (id: number) => {
    const query = `delete site where id = $1`;
    return db.query(query,[id]);
}

const findSite = (id: number) => {
    const query = `select * from site where id = $1`;
    return db.query(query,[id]);
}

export = {
    listSites,
    addSite,
    updateSite,
    deleteSite,
    findSite
}