import { ICommon } from "./common";

export interface Autorisation extends ICommon {
    id: number;
    libelle: string;
  }