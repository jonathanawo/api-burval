import { ICommon } from "./common";

export interface Intervention extends ICommon {
    id: number;
    libelle: string;
  }