import { ICommon } from "./common";

export interface Employe extends ICommon {
    id: number;
    email: string;
    motdepasse: string;
    nom: string;
    prenom: string;
    idautorisation: number;
  }