import { ICommon } from "./common";

export interface Module extends ICommon {
    id: number;
    libelle: string;
  }