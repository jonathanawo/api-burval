import { ICommon } from './common';
export interface Region extends ICommon{
    id: number;
    libelle: string;
}