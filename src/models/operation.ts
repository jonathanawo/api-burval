import { ICommon } from "./common";

export interface Operation extends ICommon {
    id: number;
    description: string;
    numbordereau: string;
    datedeclaration: Date;
    datereponse: Date;
    datearrivee: Date;
    datedebintervention: Date;
    datefinintervention: Date;
    idsite: number;
    idautomate: number;
    idintervention: number;
    idincident: number;
}