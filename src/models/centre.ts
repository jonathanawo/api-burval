import { ICommon } from "./common";

export interface Centre extends ICommon {
    id: number;
    libelle: string;
    idregion: number;
  }