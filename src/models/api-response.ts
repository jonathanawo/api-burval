export default class ApiResponse {
    error? : Error;
    data : any;

    constructor(error? : Error, data?: any) {
        this.error = error;
        this.data = data;
    }

    static withSuccess(data: any) {
        return new ApiResponse(undefined,data);
    }

    static withError(error: Error, data?: any) {
        return new ApiResponse(error, data);
    }
} 