import { ICommon } from "./common";

export interface BanqueAutomate extends ICommon {
    id: number;
    idbanque: number;
    idautomate: number;
  }