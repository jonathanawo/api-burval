import { Operation } from './operation';
import { ICommon } from "./common";

export interface OperationEmploye extends ICommon {
    id: number;
    idoperation: number;
    idemploye: number;
}