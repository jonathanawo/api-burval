import { ICommon } from "./common";

export interface Incident extends ICommon {
    id: number;
    libelle: string;
    idmodule: number;
}