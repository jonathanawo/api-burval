import { ICommon } from './common';
export interface Banque extends ICommon{
    id: number;
    libelle: string;    
}