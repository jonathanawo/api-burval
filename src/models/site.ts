import { ICommon } from "./common";

export interface Site extends ICommon {
    id: number;
    libelle: string;
    idbanque: number;
    idcentre: number;
  }