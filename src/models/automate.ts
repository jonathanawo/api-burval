import { ICommon } from "./common";

export interface Automate extends ICommon {
    id: number;
    libelle: string;
  }