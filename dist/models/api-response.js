"use strict";
exports.__esModule = true;
var ApiResponse = /** @class */ (function () {
    function ApiResponse(error, data) {
        this.error = error;
        this.data = data;
    }
    ApiResponse.withSuccess = function (data) {
        return new ApiResponse(undefined, data);
    };
    ApiResponse.withError = function (error, data) {
        return new ApiResponse(error, data);
    };
    return ApiResponse;
}());
exports["default"] = ApiResponse;
