"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listBanqueAutomates = function () {
    var query = "SELECT * FROM BANQUEAUTOMATE ORDER BY idbanque,idautomate ASC";
    return connect_1["default"].query(query);
};
var addBanqueAutomate = function (banqueAutomate) {
    var query = "INSERT INTO BANQUEAUTOMATE (idbanque,idautomate) VALUES ($1,$2) RETURNING *";
    return connect_1["default"].query(query, [banqueAutomate.idbanque, banqueAutomate.idautomate]);
};
var updateBanqueAutomate = function (id, banqueAutomate) {
    var idbanque = banqueAutomate.idbanque, idautomate = banqueAutomate.idautomate;
    var query = "UPDATE BANQUEAUTOMATE SET idbanque = $2, idautomate = $3, derniereMiseAJour = NOW() WHERE id = $1 RETURNING *";
    return connect_1["default"].query(query, [id, idbanque, idautomate]);
};
var deleteBanqueAutomate = function (id) {
    var query = "DELETE BANQUEAUTOMATE WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
var findBanqueAutomate = function (id) {
    var query = "SELECT * FROM BANQUEAUTOMATE WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
var findListAutomateByBanque = function (idBanque) {
    var query = "SELECT idAutomate FROM BANQUEAUTOMATE WHERE idbanque = $1";
    return connect_1["default"].query(query, [idBanque]);
};
var findListBanqueByAutomate = function (idAutomate) {
    var query = "SELECT idbanque FROM BANQUEAUTOMATE WHERE idautomate = $1";
    return connect_1["default"].query(query, [idAutomate]);
};
module.exports = {
    listBanqueAutomates: listBanqueAutomates,
    addBanqueAutomate: addBanqueAutomate,
    updateBanqueAutomate: updateBanqueAutomate,
    deleteBanqueAutomate: deleteBanqueAutomate,
    findBanqueAutomate: findBanqueAutomate,
    findListAutomateByBanque: findListAutomateByBanque,
    findListBanqueByAutomate: findListBanqueByAutomate
};
