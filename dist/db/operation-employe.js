"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listOperationEmployes = function () {
    var query = "select * from operationEmploye order by id ASC";
    return connect_1["default"].query(query);
};
var addOperationEmploye = function (operationUser) {
    var idoperation = operationUser.idoperation, idemploye = operationUser.idemploye;
    var query = "insert into operationUser (idoperation,idemploye) values ($1,$2)";
    return connect_1["default"].query(query, [idoperation, idemploye]);
};
var updateOperationEmploye = function (id, operationUser) {
    var idoperation = operationUser.idoperation, idemploye = operationUser.idemploye;
    var query = "update operationUser set idoperation = $2, idemploye = $3 where id = $1";
    return connect_1["default"].query(query, [id, idoperation, idemploye]);
};
var deleteOperationEmploye = function (id) {
    var query = "delete operationEmploye where id = $1";
    return connect_1["default"].query(query, [id]);
};
var findOperationEmploye = function (id) {
    var query = "select * from operationEmploye where id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listOperationEmployes: listOperationEmployes,
    addOperationEmploye: addOperationEmploye,
    updateOperationEmploye: updateOperationEmploye,
    deleteOperationEmploye: deleteOperationEmploye,
    findOperationEmploye: findOperationEmploye
};
