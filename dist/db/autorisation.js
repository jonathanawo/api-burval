"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listAutorisations = function () {
    var query = "SELECT * FROM AUTORISATION ORDER BY libelle ASC";
    return connect_1["default"].query(query);
};
var addAutorisation = function (autorisation) {
    var libelle = autorisation.libelle;
    var query = "INSERT INTO AUTORISATION (libelle) VALUES ($1) RETURNING *";
    return connect_1["default"].query(query, [libelle]);
};
var updateAutorisation = function (id, autorisation) {
    var libelle = autorisation.libelle;
    var query = "UPDATE AUTORISATION SET libelle = $2, derniereMiseAJour = NOW() WHERE id = $1 RETURNING *";
    return connect_1["default"].query(query, [id, libelle]);
};
var deleteAutorisation = function (id) {
    var query = "DELETE AUTORISATION WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
var findAutorisation = function (id) {
    var query = "SELECT * FROM AUTORISATION WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listAutorisations: listAutorisations,
    addAutorisation: addAutorisation,
    updateAutorisation: updateAutorisation,
    deleteAutorisation: deleteAutorisation,
    findAutorisation: findAutorisation
};
