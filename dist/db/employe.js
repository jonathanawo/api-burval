"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listEmployes = function () {
    var query = "SELECT * FROM EMPLOYE ORDER BY nom,prenom ASC";
    return connect_1["default"].query(query);
};
var addEmploye = function (employe) {
    var email = employe.email, motdepasse = employe.motdepasse, nom = employe.nom, prenom = employe.prenom, idautorisation = employe.idautorisation;
    var query = "INSERT INTO EMPLOYE (email,motdepasse,nom,prenom,idautorisation) VALUES ($1,$2,$3,$4,$5) RETURNING *";
    return connect_1["default"].query(query, [email, motdepasse, nom, prenom, idautorisation]);
};
var updateEmploye = function (id, employe) {
    var query = "UPDATE EMPLOYE SET EMAIL = $2, MOTDEPASSE = $3,NOM = $4,PRENOM = $5,IDAUTORISATION = $6,derniereMiseAJour = NOW() where id = $1 RETURNING *";
    return connect_1["default"].query(query, [id, employe.email, employe.motdepasse, employe.nom, employe.prenom, employe.idautorisation]);
};
var deleteEmploye = function (id) {
    var query = "delete employe where id = $1";
    return connect_1["default"].query(query, [id]);
};
var findEmploye = function (id) {
    var query = "select * from employe where id = $1";
    return connect_1["default"].query(query, [id]);
};
var findEmployeByEmail = function (email) {
    var query = "SELECT * FROM employe WHERE trim(from lower(email)) = trim(from lower($1)) LIMIT 1";
    return connect_1["default"].query(query, [email]);
};
module.exports = {
    listEmployes: listEmployes,
    addEmploye: addEmploye,
    updateEmploye: updateEmploye,
    deleteEmploye: deleteEmploye,
    findEmploye: findEmploye,
    findEmployeByEmail: findEmployeByEmail
};
