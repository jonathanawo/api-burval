"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listRegions = function () {
    var query = "select * from region order by id ASC";
    return connect_1["default"].query(query);
};
var addRegion = function (region) {
    var libelle = region.libelle;
    var query = "insert into region (libelle) values ($1)";
    return connect_1["default"].query(query, [libelle]);
};
var updateRegion = function (id, region) {
    var libelle = region.libelle;
    var query = "update region set libelle = $2 where id = $1";
    return connect_1["default"].query(query, [id, libelle]);
};
var deleteRegion = function (id) {
    var query = "delete region where id = $1";
    return connect_1["default"].query(query, [id]);
};
var findRegion = function (id) {
    var query = "select * from region where id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listRegions: listRegions,
    addRegion: addRegion,
    updateRegion: updateRegion,
    deleteRegion: deleteRegion,
    findRegion: findRegion
};
