"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listBanques = function () {
    var query = "SELECT * FROM BANQUE ORDER BY libelle ASC";
    return connect_1["default"].query(query);
};
var addBanque = function (banque) {
    var libelle = banque.libelle;
    var query = "INSERT INTO BANQUE (libelle) VALUES ($1) RETURNING *";
    return connect_1["default"].query(query, [libelle]);
};
var updateBanque = function (id, banque) {
    var libelle = banque.libelle;
    var query = "UPDATE BANQUE SET libelle = $2, derniereMiseAJour = NOW() WHERE id = $1 RETURNING *";
    return connect_1["default"].query(query, [id, libelle]);
};
var deleteBanque = function (id) {
    var query = "DELET BANQUE WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
var findBanque = function (id) {
    var query = "SELECT * FROM BANQUE WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listBanques: listBanques,
    addBanque: addBanque,
    updateBanque: updateBanque,
    deleteBanque: deleteBanque,
    findBanque: findBanque
};
