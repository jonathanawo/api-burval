"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listAutomates = function () {
    var query = "SELECT * FROM AUTOMATE ORDER BY libelle ASC";
    return connect_1["default"].query(query);
};
var addAutomate = function (automate) {
    var libelle = automate.libelle;
    var query = "INSERT INTO AUTOMATE (libelle) VALUES ($1) RETURNING *";
    return connect_1["default"].query(query, [libelle]);
};
var updateAutomate = function (id, automate) {
    var libelle = automate.libelle;
    var query = "UPDATE AUTOMATE SET libelle = $2, derniereMiseAJour = NOW() where id = $1 RETURNING *";
    return connect_1["default"].query(query, [id, libelle]);
};
var deleteAutomate = function (id) {
    var query = "DELETE AUTOMATE WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
var findAutomate = function (id) {
    var query = "SELECT * FROM AUTOMATE WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listAutomates: listAutomates,
    addAutomate: addAutomate,
    updateAutomate: updateAutomate,
    deleteAutomate: deleteAutomate,
    findAutomate: findAutomate
};
