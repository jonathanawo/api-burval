"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listSites = function () {
    var query = "select * from site order by id ASC";
    return connect_1["default"].query(query);
};
var addSite = function (site) {
    //const {libelle,idregion} = site;
    var query = "insert into site (libelle,idbanque,idcentre) values ($1,$2,$3)";
    return connect_1["default"].query(query, [site.libelle, site.idbanque, site.idcentre]);
};
var updateSite = function (id, site) {
    var libelle = site.libelle, idbanque = site.idbanque, idcentre = site.idcentre;
    var query = "update site set libelle = $2, idbanque = $3, idcentre = $4 where id = $1";
    return connect_1["default"].query(query, [id, libelle, idbanque, idcentre]);
};
var deleteSite = function (id) {
    var query = "delete site where id = $1";
    return connect_1["default"].query(query, [id]);
};
var findSite = function (id) {
    var query = "select * from site where id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listSites: listSites,
    addSite: addSite,
    updateSite: updateSite,
    deleteSite: deleteSite,
    findSite: findSite
};
