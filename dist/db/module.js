"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listModules = function () {
    var query = "SELECT * FROM MODULE WHERE libelle ASC";
    return connect_1["default"].query(query);
};
var addModule = function (module) {
    var libelle = module.libelle;
    var query = "INSERT INTO MODULE (libelle) values ($1) RETURNING *";
    return connect_1["default"].query(query, [libelle]);
};
var updateModule = function (id, module) {
    var libelle = module.libelle;
    var query = "UPDATE MODULE SET libelle = $2, derniereMiseAJour = NOW() where id = $1 RETURNING *";
    return connect_1["default"].query(query, [id, libelle]);
};
var deleteModule = function (id) {
    var query = "DELETE MODULE WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
var findModule = function (id) {
    var query = "SELECT * FROM MODULE WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listModules: listModules,
    addModule: addModule,
    updateModule: updateModule,
    deleteModule: deleteModule,
    findModule: findModule
};
