"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listOperations = function () {
    var query = "SELECT * FROM OPERATION ORDER BY datedeclaration DESC";
    return connect_1["default"].query(query);
};
var listOperationsNonDemarrees = function () {
    var query = "SELECT * FROM OPERATION WHERE datedebintervention IS NULL OR trim(datedebintervention)='' ORDER BY datedeclaration desc";
    return connect_1["default"].query(query);
};
var listOperationsDemarrees = function () {
    var query = "SELECT * FROM OPERATION WHERE datedebintervention IS NOT NULL AND (datefinintervention IS NULL OR trim(datefinintervention)='') ORDER BY datedeclaration desc";
    return connect_1["default"].query(query);
};
var listOperationsTerminees = function () {
    var query = "SELECT * FROM OPERATION WHERE datefinintervention IS NOT NULL ORDER BY datedeclaration desc";
    return connect_1["default"].query(query);
};
var addOperation = function (operation) {
    var description = operation.description, numbordereau = operation.numbordereau, datedeclaration = operation.datedeclaration, datereponse = operation.datereponse, datearrivee = operation.datearrivee, datedebintervention = operation.datedebintervention, datefinintervention = operation.datefinintervention, idsite = operation.idsite, idautomate = operation.idautomate, idintervention = operation.idintervention, idincident = operation.idincident;
    var query = "INSERT INTO OPERATION (\n        description,numbordereau,datedeclaration,datereponse,\n        datearrivee,datedebintervention,datefinintervention,\n        idsite,idautomate,\n        idintervention,idincident\n    ) VALUES ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11) RETURNING *";
    return connect_1["default"].query(query, [
        description, numbordereau, datedeclaration, datereponse,
        datearrivee, datedebintervention, datefinintervention,
        idsite, idautomate,
        idintervention, idincident
    ]);
};
var updateOperation = function (id, operation) {
    var description = operation.description, numbordereau = operation.numbordereau, datedeclaration = operation.datedeclaration, datereponse = operation.datereponse, datearrivee = operation.datearrivee, datedebintervention = operation.datedebintervention, datefinintervention = operation.datefinintervention, idsite = operation.idsite, idautomate = operation.idautomate, idintervention = operation.idintervention, idincident = operation.idincident;
    var query = "UPDATE OPERATION SET description = $2, numbordereau = $3, datedeclaration = $4 \n    ,datereponse = $5 , datearrivee = $6 , datedebintervention = $7 , datefinintervention = $8,\n    idsite = $9 , idautomate = $10 , idintervention = $11,\n    idincident = $12 where id = $1 RETURNING *";
    return connect_1["default"].query(query, [id, description, numbordereau, datedeclaration, datereponse,
        datearrivee, datedebintervention, datefinintervention,
        idsite, idautomate,
        idintervention, idincident]);
};
var updateDateArriveeOperation = function (id, datearrivee) {
    var query = "UPDATE OPERATION SET datearrivee = NOW() WHERE id = $1 RETURNING *";
};
var updateDateDebutIntervention = function (id, datedebut) {
    var query = "UPDATE OPERATION SET datedebintervention = NOW() WHERE id = $1 RETURNING *";
};
var updateDateFinIntervention = function (id, datefin) {
    var query = "UPDATE OPERATION SET datefinintervention = NOW() WHERE id = $1 RETURNING *";
};
var deleteOperation = function (id) {
    var query = "DELETE OPERATION WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
var findOperation = function (id) {
    var query = "SELECT * FROM OPERATION WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listOperations: listOperations,
    listOperationsNonDemarrees: listOperationsNonDemarrees,
    listOperationsDemarrees: listOperationsDemarrees,
    listOperationsTerminees: listOperationsTerminees,
    addOperation: addOperation,
    updateOperation: updateOperation,
    updateDateArriveeOperation: updateDateArriveeOperation,
    updateDateDebutIntervention: updateDateDebutIntervention,
    updateDateFinIntervention: updateDateFinIntervention,
    deleteOperation: deleteOperation,
    findOperation: findOperation
};
