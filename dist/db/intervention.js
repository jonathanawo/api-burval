"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listIntervention = function () {
    var query = "SELECT * FROM INTERVENTION ORDER BY libelle ASC";
    return connect_1["default"].query(query);
};
var addIntervention = function (intervention) {
    var libelle = intervention.libelle;
    var query = "INSERT INTO INTERVENTION (libelle) VALUES ($1) RETURNING *";
    return connect_1["default"].query(query, [libelle]);
};
var updateIntervention = function (id, intervention) {
    var libelle = intervention.libelle;
    var query = "UPDATE INTERVENTION SET libelle = $2 WHERE id = $1 RETURNING *";
    return connect_1["default"].query(query, [id, libelle]);
};
var deleteIntervention = function (id) {
    var query = "DELETE INTERVENTION WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
var findIntervention = function (id) {
    var query = "SELECT * FROM INTERVENTION WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listIntervention: listIntervention,
    addIntervention: addIntervention,
    updateIntervention: updateIntervention,
    deleteIntervention: deleteIntervention,
    findIntervention: findIntervention
};
