"use strict";
var pg_1 = require("pg");
// Charge la chaine de connexion automatiquement du fichier .env
var pool = new pg_1.Pool();
module.exports = {
    query: function (text, params) { return pool.query(text, params); }
};
