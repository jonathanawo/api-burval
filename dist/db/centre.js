"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listCentres = function () {
    var query = "SELECT * FROM CENTRE ORDER BY libelle ASC";
    return connect_1["default"].query(query);
};
var addCentre = function (centre) {
    var libelle = centre.libelle, idregion = centre.idregion;
    var query = "INSERT INTO CENTRE (libelle,idregion) VALUES ($1,$2) RETURNING *";
    return connect_1["default"].query(query, [libelle, idregion]);
};
var updateCentre = function (id, centre) {
    var libelle = centre.libelle, idregion = centre.idregion;
    var query = "UPDATE CENTRE SET libelle = $2, idregion = $3, derniereMiseAJour = NOW() WHERE id = $1 RETURNING *";
    return connect_1["default"].query(query, [id, libelle, idregion]);
};
var deleteCentre = function (id) {
    var query = "DELETE CENTRE WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
var findCentre = function (id) {
    var query = "SELECT * FROM CENTRE WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listCentres: listCentres,
    addCentre: addCentre,
    updateCentre: updateCentre,
    deleteCentre: deleteCentre,
    findCentre: findCentre
};
