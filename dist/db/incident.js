"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listIncidents = function () {
    var query = "SELECT * FROM INCIDENT ORDER BY libelle ASC";
    return connect_1["default"].query(query);
};
var addIncident = function (incident) {
    var query = "INSERT INTO INCIDENT (libelle,idmodule) VALUES ($1,$2) RETURNING *";
    return connect_1["default"].query(query, [incident.libelle, incident.idmodule]);
};
var updateIncident = function (id, incident) {
    var libelle = incident.libelle, idmodule = incident.idmodule;
    var query = "UPDATE INCIDENT SET libelle = $2, idmodule = $3, derniereMiseAJour = NOW() WHERE id = $1";
    return connect_1["default"].query(query, [id, libelle, idmodule]);
};
var deleteIncident = function (id) {
    var query = "DELETE INCIDENT WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
var findIncident = function (id) {
    var query = "SELECT * FROM INCIDENT WHERE id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listIncidents: listIncidents,
    addIncident: addIncident,
    updateIncident: updateIncident,
    deleteIncident: deleteIncident,
    findIncident: findIncident
};
