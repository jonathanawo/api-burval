"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var connect_1 = __importDefault(require("./connect"));
var listEmployeAutorisation = function () {
    var query = "select * from employeautorisation order by id ASC";
    return connect_1["default"].query(query);
};
var addEmployeAutorisation = function (employeAutorisation) {
    var idemploye = employeAutorisation.idemploye, idautorisation = employeAutorisation.idautorisation;
    var query = "insert into employeautorisation (idemploye,idautorisation) values ($1,$2)";
    return connect_1["default"].query(query, [idemploye, idautorisation]);
};
var updateEmployeAutorisation = function (id, employeAutorisation) {
    var idemploye = employeAutorisation.idemploye, idautorisation = employeAutorisation.idautorisation;
    var query = "update employeautorisation set idemploye = $2,idautorisation = $3 where id = $1";
    return connect_1["default"].query(query, [id, idemploye, idautorisation]);
};
var deleteEmployeAutorisation = function (id) {
    var query = "delete employeautorisation where id = $1";
    return connect_1["default"].query(query, [id]);
};
var findEmployeAutorisation = function (id) {
    var query = "select * from employeautorisation where id = $1";
    return connect_1["default"].query(query, [id]);
};
module.exports = {
    listEmployeAutorisation: listEmployeAutorisation,
    addEmployeAutorisation: addEmployeAutorisation,
    updateEmployeAutorisation: updateEmployeAutorisation,
    deleteEmployeAutorisation: deleteEmployeAutorisation,
    findEmployeAutorisation: findEmployeAutorisation
};
