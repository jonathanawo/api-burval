"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
exports.__esModule = true;
var validator_1 = __importDefault(require("validator"));
function asyncWrapper(callback) {
    return function (req, res, next) {
        callback(req, res, next)["catch"](function (err) {
            console.log(err.message);
            var code = 9000; // autres
            if (validator_1["default"].isJSON(err.message)) {
                var errorJson = JSON.parse(err.message);
                if (errorJson.type === 'db') {
                    code = 8000; // erreur base de données
                }
            }
            // TODO: logger l'erreur ici
            next(new Error(code + ''));
        });
    };
}
exports["default"] = asyncWrapper;
